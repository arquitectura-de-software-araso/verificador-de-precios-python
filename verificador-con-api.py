from tkinter import *
from PIL import ImageTk, Image

ventana = Tk()
ventana.geometry("1000x815")
ventana.title("Verificador de Precios")

width = ventana.winfo_screenwidth()
height = ventana.winfo_screenheight()

ancho_forma = ventana.winfo_reqwidth()
alto_forma = ventana.winfo_reqheight()

import requests

codigo=""
def key_pressed(event):
  global codigo 
  if event.keysym == "Return":
    print(codigo)
    URL = "http://localhost/api/index.php?codigo=" + codigo
    respuesta = requests.get(URL)

    datos = respuesta.json()
    print(datos["nombre"])
    print(datos["precio"])
    print(datos["descripcion"])
    print(datos["imagen"])
  else:
    loadImg("./img" + datos["imagen"])
    labelProducto.config(text="Nombre del producto no existe")
    labelPrecio.config(text="Precio del producto no existe")

def loadImg(imgPath):
  # imagen
  # PNG, JPEG and GIF
  render = ImageTk.PhotoImage(Image.open(imgPath))
  img = Label(ventana, image=render, width=500, height=500)
  img.image = render

loadImg("barcode-scan.gif")

if datos["status"] == 200:
  loadImg("./img" + datos["imagen"])
  

#El Mainloop va a lo ultimo
label = Label(ventana,text="Verificador de Precios",font=("Courier", 22))
label.pack()
label.place(x=ventana.winfo_width()/2 - label.winfo_width()/2, y=0)

#Label Producto
labelProducto = Label(ventana,text="Width" +" "+ str(ventana.winfo_reqwidth()))
labelProducto.pack()
labelProducto.place(x=10, y=150)

#Label Precio
labelPrecio = Label(ventana,text="Height" +" "+ str(ventana.winfo_reqheight()))
labelPrecio.pack()
labelPrecio.place(x=10, y=195)

ventana.mainloop()
