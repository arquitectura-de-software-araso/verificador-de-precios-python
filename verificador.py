from tkinter import *

ventana = Tk()
ventana.geometry("1000x815")
ventana.title("Verificador de Precios")

width = ventana.winfo_screenwidth()
height = ventana.winfo_screenheight()

ancho_forma = ventana.winfo_reqwidth()
alto_forma = ventana.winfo_reqheight()

ventana.update()

#El Mainloop va a lo ultimo
label = Label(ventana,text="Verificador de Precios",font=("Courier", 22))
label.pack()
ventana.update()
label.place(x=ventana.winfo_width()/2 - label.winfo_width()/2, y=0)

#Label 2
label2 = Label(ventana,text="Width" +" "+ str(ventana.winfo_reqwidth()))
label2.pack()
ventana.update()
label2.place(x=0, y=50)

#Label 3
label3 = Label(ventana,text="Height" +" "+ str(ventana.winfo_reqheight()))
label3.pack()
ventana.update()
label3.place(x=0, y=100)

ventana.mainloop()
